# BÀI TEST TUYỂN PHP KAN-TEK.COM #


#  TEST KIẾN THỨC LIÊN QUAN: **PHPExcel, API, CURL, JS**. #


## I.		TÀI NGUYÊN ##
*	Thông tin FTP, Database(đã import) trên server kan-tek, file sql(chỉ dùng để tham khảo).
*	1 file excel chứa danh sách user để test, list danh sách này chỉ **1 phần nhỏ** trong database trên kan-tek.


## II.		MÔ TẢ CÔNG VIỆC ##
*	Xây dựng server kan-tek **CHỈ** để làm **JSON webservice** xử lý lấy thông tin bảng users, update thông tin user(nếu cần) và trả kết quả về client xử lý, xuất ra trình duyệt.

	VD: 
	```
	1 JSON webservice cần xây dựng: https://output.jsbin.com/dekafa.json
	```
	
*	Trên localhost xây dựng Form cho phép duyệt và đọc file Excel, sau đó gọi **(CURL)** đến JSON webservice ở trên để lấy xuất ra(table) được thông tin tất cả các user **vừa** có trong file excel và **vừa** có trong table users và vừa có group_id là **3**.(group_id là 1 cột trong table users).

	VD: 
	```
	curl -X -POST --data "param1=value1&param2=value2" http://username.test.kan-tek.com/getuser
	```
	
*	Cho phép update group_id những users đó

	VD: 
	```
	curl -X -POST --data “user_id=10&group_id=4” http://username.test.kan-tek.com/updateuser
	```
	
	*Cú pháp trên update group_id bằng 4 của user có user_id là 10*

*	Xuất ra file Excel tương tự file gốc nhưng chỉ còn những user có group_id là 3

*	Validate Upload

*	Viết tối ưu nhất có thể


## III.	CHÚ Ý ##

*	Gửi source chứa JSON webservice và source client, có demo online càng tốt.
*	Xem trong thư mục sample hình mẫu
*	Hiểu thật kỹ công việc cần làm trước khi bắt đầu
*	Không nhất thiết làm hết yêu cầu
*	Các cú pháp curl ở trên dạng terminal để tham khảm. Tham khảm CURL trong PHP http://bit.ly/1gux5jD để thực hiện trong bài test.
*	Nếu bạn có host hoặc server riêng thì có thể tạo JSON webservice riêng không cân phải trên kan-tek